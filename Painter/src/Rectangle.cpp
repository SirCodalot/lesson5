#include "Rectangle.h"

namespace myShapes
{

	Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name): Polygon(type, name)
	{
		_points.push_back(a);
		_points.push_back(Point(a.getX() + length, a.getY() + width));
		_length = length;
		_width = width;
	}

	Rectangle::~Rectangle()
	{
		
	}

	double Rectangle::getLength() const
	{
		return _length;
	}

	double Rectangle::getWidth() const
	{
		return _width;
	}

	double Rectangle::getArea() const
	{
		return _length * _width;
	}

	double Rectangle::getPerimeter() const
	{
		return 2 * (_length + _width);
	}

	void Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
	{
		unsigned char WHITE[] = { 255, 255, 255 };
		board.draw_rectangle(_points[0].getX(), _points[0].getY(),
			_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
	}

	void Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
	{
		unsigned char BLACK[] = { 0, 0, 0 };
		board.draw_rectangle(_points[0].getX(), _points[0].getY(),
			_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
	}

}