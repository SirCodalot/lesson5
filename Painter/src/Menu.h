#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>

class Menu
{
public:

	Menu();
	~Menu();

	// more functions..
	void setMain() const;
	void setShapeCreation() const;
	void setDeleteAll() const;
	void setShapeSelection() const;

	void createCircle() const;
	void createArrow() const;
	void createTriangle() const;
	void createRectangle() const;

private:
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
	vector<Shape*>* _shapes;
};

void moveShape(Shape* shape, cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
void printDetails(Shape* shape);