#include "Point.h"

Point::Point(double x, double y)
{
	_x = x;
	_y = y;
}

Point::Point(const Point& other)
{
	_x = other._x;
	_y = other._y;
}

Point::~Point()
{
}

Point Point::operator+(const Point& other) const
{
	return *new Point(_x + other._x, _y + other._y);
}

Point& Point::operator+=(const Point& other)
{
	_x += other._x;
	_y += other._y;

	return *this;
}

double Point::getX() const
{
	return _x;
}

double Point::getY() const
{
	return _y;
}

double Point::distance(const Point& other) const
{
	double width = abs(_x - other._x);
	double height = abs(_y - other._y);

	return sqrt(pow(width, 2) + pow(height, 2));
}
