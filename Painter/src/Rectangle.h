#pragma once
#include "Polygon.h"

namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{
	public:
		// There's a need only for the top left corner 
		Rectangle(const Point& a, double length, double width, const string& type, const string& name);
		virtual ~Rectangle();

		double getLength() const;
		double getWidth() const;

		// override functions if need (virtual + pure virtual)
		double getArea() const override;
		double getPerimeter() const override;

		void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board) override;
		void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board) override;
	private:
		double _length;
		double _width;
	};
}