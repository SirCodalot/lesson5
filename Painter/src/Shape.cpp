#include "Shape.h"

using namespace std;

Shape::Shape(const string& name, const string& type)
{
	_name = name;
	_type = type;
}

string Shape::getName() const
{
	return _name;
}

string Shape::getType() const
{
	return _type;
}

void Shape::printDetails() const
{
	cout << getType() << "\t" << getName() << "\t" << getArea() << "\t" << getPerimeter() << endl;
}
