#include "Polygon.h"

using namespace std;

Polygon::Polygon(const string& type, const string& name): Shape(type, name)
{
	_points = *new vector<Point>();
}

Polygon::~Polygon()
{
	
}

void Polygon::move(const Point& other)
{
	for (int i = 0; i < _points.size(); i++)
		_points[i] += other;
}
