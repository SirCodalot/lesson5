#include "Menu.h"

Menu::Menu()
{
	_board = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
	_shapes = new vector<Shape*>();

	setMain();
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

void Menu::setMain() const
{
	int input = 0;

	while (input != 3)
	{
		system("cls");
		cout	<< "Enter 0 to add a new shape." << endl
				<< "Enter 1 to modify or get information from a current shape." << endl
				<< "Enter 2 to delete all of the shapes" << endl
				<< "Enter 3 to exit" << endl;
		cin >> input;

		Shape* shape;
		switch (input)
		{
			case 0:
				setShapeCreation();
				break;
			case 1:
				setShapeSelection();
				break;
			case 2:
				setDeleteAll();
				break;
		}
	}
}

void Menu::setDeleteAll() const
{
	for (unsigned int i = 0; i < _shapes->size(); i++)
		(*_shapes)[i]->clearDraw(*_disp, *_board);
	_shapes->clear();
}

void Menu::setShapeSelection() const
{
	system("cls");
	
	int input = 0;
	int index = 0;
	do
	{
		for (int i = 0; i < _shapes->size(); i++)
		{
			Shape* shape = (*_shapes)[i];
			cout << "Enter " << i << " for " << shape->getName() << "(" << shape->getType() << ")" << endl;
		}
		cin >> index;
	}  while (index < 0 || index > _shapes->size());

	Shape* shape = (*_shapes)[index];

	do
	{
		cout	<< "Enter 0 to move the shape." << endl
				<< "Enter 1 to get its details." << endl
				<< "Enter 2 to remove the shape." << endl;
		cin >> input;
	} while (input< 0 || input > 2);

	switch (input)
	{
		case 0:
			moveShape(shape, *_disp, *_board);
			for (int i = 0; i < _shapes->size(); i++)
			{
				(*_shapes)[i]->clearDraw(*_disp, *_board);
				(*_shapes)[i]->draw(*_disp, *_board);
			}
			break;
		case 1:
			printDetails(shape);
			break;
		case 2:
			shape->clearDraw(*_disp, *_board);
			_shapes->erase(_shapes->begin() + index);
			for (int i = 0; i < _shapes->size(); i++)
			{
				(*_shapes)[i]->clearDraw(*_disp, *_board);
				(*_shapes)[i]->draw(*_disp, *_board);
			}
			break;
	}
}

void moveShape(Shape* shape, cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	system("cls");

	int x = 0;
	int y = 0;

	cout << "Please enter the X moving scale: ";
	cin >> x;
	cout << "Please enter the Y moving scale: ";
	cin >> y;

	shape->clearDraw(disp, board);
	shape->move(Point(x, y));
	shape->draw(disp, board);
}

void printDetails(Shape* shape)
{
	shape->printDetails();
	system("pause");
}

void Menu::setShapeCreation() const
{
	int input = 0;
	do
	{
		system("cls");
		cout	<< "Enter 0 to add a circle." << endl
				<< "Enter 1 to add an arrow." << endl
				<< "Enter 2 to add a triangle." << endl
				<< "Enter 3 to add a rectangle."<< endl;
		cin >> input;
	} while (input < 0 || input > 3);

	switch (input)
	{
		case 0:
			createCircle();
			break;
		case 1:
			createArrow();
			break;
		case 2:
			createTriangle();
			break;
		case 3:
			createRectangle();
			break;
	}
}

void Menu::createCircle() const
{
	int x = 0;
	int y = 0;
	int r = 0;
	string name = "";
	
	cout << "Please enter X:" << endl;
	cin >> x;

	cout << "Please enter Y:" << endl;
	cin >> y;

	cout << "Please enter radius:" << endl;
	cin >> r;

	cout << "Please enter the name of the shape:" << endl;
	cin >> name;

	Circle* circle = new Circle(Point(x, y), r, "Circle", name);
	circle->draw(*_disp, *_board);
	_shapes->push_back(circle);
}

void Menu::createArrow() const
{
	int x1 = 0;
	int y1 = 0;
	int x2 = 0;
	int y2 = 0;
	string name = "";

	cout << "Enter the X of point number: 1" << endl;
	cin >> x1;
	cout << "Enter the Y of point number: 1" << endl;
	cin >> y1;

	cout << "Enter the X of point number: 2" << endl;
	cin >> x2;
	cout << "Enter the Y of point number: 2" << endl;
	cin >> y2;

	cout << "Enter the name of the shape:" << endl;
	cin >> name;

	Arrow* arrow = new Arrow(Point(x1, y1), Point(x2, y2), "Arrow", name);
	arrow->draw(*_disp, *_board);
	_shapes->push_back(arrow);
}

void Menu::createTriangle() const
{
	int x1 = 0;
	int y1 = 0;
	int x2 = 0;
	int y2 = 0;
	int x3 = 0;
	int y3 = 0;
	string name = "";

	cout << "Enter the X of point number: 1" << endl;
	cin >> x1;
	cout << "Enter the Y of point number: 1" << endl;
	cin >> y1;

	cout << "Enter the X of point number: 2" << endl;
	cin >> x2;
	cout << "Enter the Y of point number: 2" << endl;
	cin >> y2;

	cout << "Enter the X of point number: 3" << endl;
	cin >> x3;
	cout << "Enter the Y of point number: 3" << endl;
	cin >> y3;

	cout << "Enter the name of the shape:" << endl;
	cin >> name;

	Triangle* triangle = new Triangle(Point(x1, y1), Point(x2, y2), Point(x3, y3), "Triangle", name);
	triangle->draw(*_disp, *_board);
	_shapes->push_back(triangle);
}

void Menu::createRectangle() const
{
	int x;
	int y;
	int l;
	int w;
	string name = "";

	cout << "Enter the X of the top left corner:" << endl;
	cin >> x;
	cout << "Enter the Y of the top left corner:" << endl;
	cin >> y;

	cout << "Enter the length of the shape:" << endl;
	cin >> l;
	cout << "Enter the width of the shape:" << endl;
	cin >> w;

	cout << "Enter the name of the shape:" << endl;
	cin >> name;

	myShapes::Rectangle* rectangle = new myShapes::Rectangle(Point(x, y), l, w, "Rectangle", name);
	rectangle->draw(*_disp, *_board);
	_shapes->push_back(rectangle);
}